<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Request;
use View;

class UserController extends Controller
{
    public static $var = 1;

    public function index()
    {
        $name = Request::input('search', 'Nema');
        $var = session('var', 0);
        $var ++;
        session(['var'=>$var]);

        return view('user.info', ['name'=>$name, 'brojac' =>$var]);
    }
}
