<?php

namespace App\Http\Controllers;

use App\Logic\Auth\Authorize;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    public function index()
    {
        return view('user.login');
    }

    public function loginUser(Request $request)
    {
        $username = $request->input('username', null);
        $password = $request->input('password', null);
        if (Authorize::checkLogin($username, $password))
        {
            Authorize::login($username, $password);
            return Redirect::action('RestaurantController@index');
        }
        else{
            return view('user.login');
        }

    }

    public function checkLogin()
    {
        if (Authorize::checkLogin(Request::input('username', null),
            Request::input('password, null')))
        {
            return true;
        }
        else{
            return false;
        }

    }

    public function logoutUser()
    {
        Authorize::logout();
        return Redirect::action('UserController@index');
    }
}
