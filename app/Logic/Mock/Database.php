<?php
/**
 * Created by PhpStorm.
 * User: popina
 * Date: 04.01.2018.
 * Time: 18:57
 */

namespace App\Logic\Mock;

use App\Models\User;


class Database
{
    public static function initialize()
    {
        if (!session()->has('initialized'))
        {
            $user = new User('mama', '1', 'Ђорђе', 'Живановић', '064/111111111', 'Лопатањ б.б', 'dz@d.oom');
            session(['user_table'=> ['mama' => $user]]);
            session(['initialized'=>true]);
        }
    }
}