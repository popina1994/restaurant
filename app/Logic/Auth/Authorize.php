<?php
/**
 * Created by PhpStorm.
 * User: popina
 * Date: 03.01.2018.
 * Time: 20:50
 */

namespace App\Logic\Auth;
use App\Models\User;

class Authorize
{
    public static function  checkLogin($username=null, $password = null)
    {
        if ($username == null)
        {
            $user = session('user');
            if ($user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        $user_table = session('user_table');
        if (($user_table == null) || !array_key_exists($username, $user_table))
        {
            return false;
        }
        $user = $user_table[$username];
        if ($user->getPassword() == $password)
        {
            return true;
        }

        return false;
    }

    public static function login($username, $password)
    {
        $user_table = session('user_table');
        $user = $user_table[$username];
        session(['user' =>$user]);
    }

    public static function getUser()
    {
        $t = session('user');;
        return $t;
    }

    public static function logout()
    {
        session()->flush();
    }
}