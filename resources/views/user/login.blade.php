@extends('layout.master')

@section('title', 'login')
@section('content')

    <div class="space-medium">
        <div class="container-fluid">
                    <div class="account-holder">
                        <!--login-form-->
                        <h3 align="center"> Улогуј се у Ресторанџија </h3>
                        <br>
                        <div class="row">
                            <form action="{{action('LoginController@loginUser')}}" method="post">
                                {{ csrf_field() }}
                                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12 col-md-offset-3">
                                    <div class="form-group">
                                        <label class="control-label required" for="email">Email<sup
                                                    style="color:red">*</sup></label>
                                        <input id="email" name="username" type="text" class="form-control"
                                               placeholder="Email адреса" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12 col-md-offset-3">
                                    <div class="form-group">
                                        <label class="control-label required" for="email">Шифра<sup
                                                    style="color:red">*</sup></label>
                                        <input id="password" name="password" type="password" class="form-control"
                                               placeholder="Шифра" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12 col-md-offset-3" >
                                    <button class="btn btn-primary btn-block">Улоуј се</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!--/.login-form-->
                <!--sing-up-form-->
                <!--
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="account-holder">
                        <h3>Signup With Today’s Fashion</h3>
                        <br>

                        <div class="row">
                            <form>
                                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label required" for="name"> Name<sup
                                                    style="color:red">*</sup></label>
                                        <input id="name" name="name" type="text" class="form-control"
                                               placeholder="Enter Your NAme">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label required" for="phone">Phone<sup
                                                    style="color:red">*</sup></label>
                                        <input id="phone" name="phone" type="text" class="form-control"
                                               placeholder="Enter Mobile Number">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label required" for="email">Email<sup
                                                    style="color:red">*</sup></label>
                                        <input id="email" name="email" type="text" class="form-control"
                                               placeholder="Enter Email Address">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label required" for="password">Password<sup
                                                    style="color:red">*</sup></label>
                                        <input id="password" name="password" type="password" class="form-control"
                                               placeholder="password">
                                    </div>
                                    <div class="mb30">
                                        <p>Already have an account?   <a href="#">Login</a></p>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                    <button class="btn btn-primary btn-block">Register</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

                -->


    </div>
    </div>

@stop
