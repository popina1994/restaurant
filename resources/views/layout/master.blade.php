<html>
{{ Html::style('css/style.css') }}
{{ Html::script('js/jquery-3.2.1.js') }}
{{ Html::script('js/bootstrap.js') }}
{{ Html::script('js/owl.carousel.js') }}
<head>
    <title>@yield('title')</title>
</head>
<body>
@include('layout.master_header')
<div class="container">
    @yield('content')
</div>

@include('layout.master_footer')

</body>
</html>