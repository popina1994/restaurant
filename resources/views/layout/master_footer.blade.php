<footer class="navbar-fixed-bottom navbar-inverse">
    <div class="container-fluid">
        <ul class="nav navbar-nav navbar-left">
            <li><a href="#">О нама</a></li>
            <li><a href="#">Апликација      </a></li>
            <li><a href="#">Web analytics</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#" >© 2017 Ђорђе Живановић. </a></li>
            <li><a href="#" >Услови коришћења</a></li>
            <li><a href="#">Приватност</a></li>
        </ul>
    </div>

</footer>