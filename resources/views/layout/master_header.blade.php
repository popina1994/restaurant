<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Почетна</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <li {{(request()->path() == 'restaurant') ? 'class=active' : '' }}>
                    <a href="{{URL::to('/restaurant')}}">Ресторани</a></li>
                <li {{(request()->path() == 'meal') ? 'class=active' : "" }}>
                    <a  href="{{URL::to('/meal')}}">Јела</a></li>
                @if (\App\Logic\Auth\Authorize::checkLogin())
                    <li {{(request()->path() == 'order') ? 'class=active' : "" }}>
                        <a href="{{URL::to('/order')}}">Порџубине</a></li>
                @endif
                <li><a href="#">Контакт </a></li>

            </ul>
            <form class="navbar-form navbar-left">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Претражи" name="search">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle btn btn-dark" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-user"></span>

                        @if (\App\Logic\Auth\Authorize::checkLogin())
                            {{ \App\Logic\Auth\Authorize::getUser()->getFirstName() }}
                        @else
                            Нерегистровани
                        @endif

                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        @if (\App\Logic\Auth\Authorize::checkLogin())
                            <li><a href="{{URL::to('user')}}">Лични подаци</a></li>
                            <li><a href="#">Уреди личне податке</a></li>
                            <li><a href="#">Измени лозинку</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Подешавања</a></li>
                        @else
                            <li><a href="{{URL::to('login')}}">Улогуј се</a></li>
                            <li><a href="#">Региструј се</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Подешавања</a></li>
                        @endif
                    </ul>
                </li>
                <li>
                    <a href="#" class="btn  btn-dark" onclick="document.getElementById('formLogOut').submit();">
                        <span class="glyphicon glyphicon-log-out"></span> Одјави се
                    </a>
                </li>
                @if (\App\Logic\Auth\Authorize::checkLogin())
                    <li><a href="#" class="btn  btn-dark">
                            <span class="glyphicon glyphicon-shopping-cart"></span> Корпа
                        </a>
                    </li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>


<form action="{{action('LoginController@logoutUser')}}" method="post" class="navbar-form" id="formLogOut">
    {{ csrf_field() }}
</form>